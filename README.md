# mvi-sp
Cílem je natrénovat neuronovu síť, která bude se slušnou úspěšností klasifikovat kola a motorky. Optimistickým cílem je 70\% úspěšnost.
Prvním krokem bylo sehnat si data. Dále vybrat vhodné metody pro klasifikaci.

Struktura repozitře:

vehicles - ukázková data (celá data dostupná zde: https://drive.google.com/drive/folders/1ImsTLyih0tFu20w7jNwLsWz_N3Sc6HNK?usp=sharing)

semestralka.ipnb - notebook s neuronovou sítí

mode066 - natrénovaný model

pretrained - skript použitý k dotrénování předtrénované neuronové sítě

report.pdf - report k semestrální práci
